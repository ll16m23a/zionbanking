from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from app import db


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(10))
    password = db.Column(db.String(50))
    firstname = db.Column(db.String(10))
    lastname = db.Column(db.String(10))
    age = db.Column(db.Integer)
    address = db.Column(db.String(50))
    accountype = db.Column(db.String(10))
    contactnum = db.Column(db.String(12))
    gender = db.Column(db.String(10))
    balance = db.Column(db.Float)
    overdraft = db.Column(db.Float)
    transfers = db.relationship('Transfers', backref='Data', lazy='dynamic', foreign_keys='Transfers.transferreciever')
    transfers1 = db.relationship('Transfers', backref='Data1', lazy='dynamic', foreign_keys='Transfers.transfersender')
    withdrawls = db.relationship('Withdrawls', backref='Information', lazy='dynamic')
    deposits = db.relationship('Deposits', backref='Info', lazy='dynamic')
    def __repr__(self):
            return '' % (self.id, self.username, self.password, self.firstname, self.lastname, self.age, self.accountype, self.contactnum, self.gender, self.balance)

class Transfers(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    message = db.Column(db.String)
    amountsent = db.Column(db.Float)
    transferreciever = db.Column(db.Integer, db.ForeignKey('user.id'))
    transfersender = db.Column(db.Integer, db.ForeignKey('user.id'))
    def __repr__(self):
            return '' % (self.id, self.date, self.transferreciever, self.transfersender, self.message, self.amountsent)

class Withdrawls(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    withdrawl = db.Column(db.Float)
    date = db.Column(db.DateTime)
    withdrawluser = db.Column(db.Integer, db.ForeignKey('user.id'))
    def __repr__(self):
            return '' % (self.id, self.withdrawl, self.date, self.withdrawluser)

class Deposits(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    deposit = db.Column(db.Float)
    date = db.Column(db.DateTime)
    deposituser = db.Column(db.Integer, db.ForeignKey('user.id'))
    def __repr__(self):
            return '' % (self.id, self.deposit, self.date, self.deposituser)
