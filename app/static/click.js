function click() {
    if(typeof(Storage) !== "undefined") {
        if (localStorage.clickcount) {
            localStorage.clickcount = Number(localStorage.clickcount)+1;
        } else {
            localStorage.clickcount = 1;
        }
        document.getElementById("count").innerHTML = "You have got your location " + localStorage.clickcount + " time(s).";
    } else {
        document.getElementById("count").innerHTML = "Not supported.";
    }
}