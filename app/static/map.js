var x = document.getElementById("demo");

function map() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        x.innerHTML = "Not supported.";
    }
}

function showPosition(position) {
    var latlon = position.coords.latitude + "," + position.coords.longitude;
    var img_url = "https://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=14&size=400x300&key=AIzaSyD4lAHrUNFmM4Xk88XUggR1o6sCztoFmKg&signature=mBVF71-01WK78d1kbPhS9A2x7c4";
    document.getElementById("map").innerHTML = "Your position is: " + latlon;
}


function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "Geolocation has been denied from you."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Denied."
            break;
        case error.TIMEOUT:
            x.innerHTML = "Time out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "Error."
            break;
    }
}
