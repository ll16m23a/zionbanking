import logging
from logging.handlers import RotatingFileHandler
from datetime import datetime
from flask import render_template, flash, request, redirect, url_for, jsonify, request, make_response
from app import app
from app import db
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, IntegerField, TextAreaField, FloatField
from wtforms.validators import InputRequired, Length
from .models import User
from .models import Transfers
from .models import Withdrawls
from .models import Deposits
from flask_bootstrap import Bootstrap
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from wtforms import SelectField

#Error log for app
file_handler = RotatingFileHandler('errorlog.txt')
file_handler.setLevel(logging.ERROR)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
file_handler.setFormatter(formatter)
app.logger.addHandler(file_handler)

Bootstrap(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

@app.errorhandler(500)
def internal_error(exception):
    app.logger.error(exception)
    return render_template('500.html'), 500

@app.errorhandler(404)
def internal_error(exception):
    app.logger.error(exception)
    return render_template('404.html'), 404

@login_manager.user_loader
def load_user(User_id):
    return User.query.get(int(User_id))
#all forms below for different parts of app
class LoginForm(FlaskForm):
    username = StringField('Username:', validators= [InputRequired(), Length(min=5, max=10)])
    password = PasswordField('Password:', validators= [InputRequired(), Length(min=5, max=50)])

class TransferForm(FlaskForm):
    reciever = IntegerField('Reciever ID:', validators= [InputRequired()])
    amount = FloatField('Amount:', validators= [InputRequired()])
    message = TextAreaField('Message:')

class WithdrawForm(FlaskForm):
    amount = FloatField('Amount:', validators= [InputRequired()])

class DepositForm(FlaskForm):
    amount = FloatField('Amount:', validators= [InputRequired()])

class RegisterForm(FlaskForm):
    username = StringField('Username:* ', validators= [InputRequired(), Length(min=5, max=10)])
    password = PasswordField('Password:*', validators= [InputRequired(), Length(min=5, max=50)])
    password2 = PasswordField('Repeat Password:*', validators= [InputRequired(), Length(min=5, max=50)])
    firstname = StringField('First Name:*', validators= [InputRequired(), Length(min=4, max=10)])
    lastname = StringField('Last Name:*', validators= [InputRequired(), Length(min=4, max=10)])
    age = IntegerField('Age:*')
    address = StringField('Address:*', validators= [InputRequired(), Length(min=10, max=50)])
    accountype = SelectField('Account Type:*', validators= [InputRequired()], choices=[('Student', 'Student'), ('Child', 'Child'), ('Savings', 'Savings')])
    contactnum = StringField('Contact Number:',validators=  [Length(min=10, max=12)])
    gender = SelectField('Gender:', choices=[('Male', 'Male'), ('Female', 'Female')])
    startingbalance = SelectField('Starting Balance:*', validators= [InputRequired()], choices=[('0', '£0'), ('100', '£100'), ('500', '£500'), ('1000', '£1,000'), ('5000', '£5,000'), ('10000', '£10,000')])

class changePass(FlaskForm):
    password = PasswordField('Password:*', validators= [InputRequired(), Length(min=5, max=50)])
    password2 = PasswordField('Repeat Password:*', validators= [InputRequired(), Length(min=5, max=50)])

class changeAge(FlaskForm):
    age = IntegerField('Age:*')

class changeAddress(FlaskForm):
    address = StringField('Address:*', validators= [InputRequired(), Length(min=10, max=50)])

class changeNum(FlaskForm):
    contactnum = StringField('Contact Number*:',validators=  [Length(min=10, max=12)])

@app.route('/changepass', methods=['GET', 'POST'])
@login_required
def changepass():
    form = changePass()
    if form.validate_on_submit():
        #if the password in field 1 isnt same as field 2 throw error page
        if form.password.data != form.password2.data:
            return redirect(url_for('notsame2'))
        else:
        #otherwise update password for current logged in user
            update = User.query.filter_by(id = current_user.get_id()).update(dict(password = form.password.data))
            db.session.commit()
            return redirect(url_for('successu'))
    return render_template('changepass.html', title=':: Settings', form=form)

@app.route('/changeage', methods=['GET', 'POST'])
@login_required
def changeage():
    form = changeAge()
    if form.validate_on_submit():
        #update password for current logged in user
            update = User.query.filter_by(id = current_user.get_id()).update(dict(age = form.age.data))
            db.session.commit()
            return redirect(url_for('successu'))
    return render_template('changeage.html', title=':: Settings', form=form)


@app.route('/changeAddress', methods=['GET', 'POST'])
@login_required
def changeaddress():
    form = changeAddress()
    if form.validate_on_submit():
        #update address for current logged in user
            update = User.query.filter_by(id = current_user.get_id()).update(dict(address = form.address.data))
            db.session.commit()
            return redirect(url_for('successu'))
    return render_template('changeaddress.html', title=':: Settings', form=form)
#render a success page
@app.route('/successu')
@login_required
def successu():
    return render_template('successu.html', title=':: Settings')

@app.route('/changeNum', methods=['GET', 'POST'])
@login_required
def changenum():
    form = changeNum()
    if form.validate_on_submit():
        #update number for current logged in user
            update = User.query.filter_by(id = current_user.get_id()).update(dict(contactnum = form.contactnum.data))
            db.session.commit()
            return redirect(url_for('successu'))
    return render_template('changenum.html', title=':: Settings', form=form)


@app.route('/settings')
@login_required
def settings():
    #query to find all information in table User for the current logged in user
    query = User.query.filter_by(id = current_user.get_id()).all()
    return render_template('settings.html', title=':: Settings', query = query)


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        #checks to see if username already exists and if so error is thrown
        testuser = form.username.data
        test = User.query.filter_by(username = testuser).first()
        if test:
            return redirect(url_for('exists'))
        #if password in fild 1 doesnt match in field 2 then throw error
        elif (form.password.data != form.password2.data):
            return redirect(url_for('notsame'))
        #saves to database accordingly to the account type chosen so the right overdraft is saved
        else:
            if form.accountype.data == "Student":
                user = User(username=form.username.data, password=form.password.data, firstname = form.firstname.data, lastname = form.lastname.data, age = form.age.data, address = form.address.data, accountype = form.accountype.data, contactnum = form.contactnum.data, gender = form.gender.data, balance = form.startingbalance.data, overdraft = "1000")
                db.session.add(user)
                db.session.commit()
                return redirect(url_for('login'))
            elif form.accountype.data == "Child":
                user = User(username=form.username.data, password=form.password.data, firstname = form.firstname.data, lastname = form.lastname.data, age = form.age.data, address = form.address.data, accountype = form.accountype.data, contactnum = form.contactnum.data, gender = form.gender.data, balance = form.startingbalance.data, overdraft = "1000")
                db.session.add(user)
                db.session.commit()
                return redirect(url_for('login'))
            else:
                user = User(username=form.username.data, password=form.password.data, firstname = form.firstname.data, lastname = form.lastname.data, age = form.age.data, address = form.address.data, accountype = form.accountype.data, contactnum = form.contactnum.data, gender = form.gender.data, balance = form.startingbalance.data, overdraft = "500")
                db.session.add(user)
                db.session.commit()
                return redirect(url_for('login'))
    return render_template('register.html', title=':: Register', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    #makes and gets cookies
    resp = make_response('Cookie')
    resp.set_cookie('Login')
    Login =  request.cookies.get('Login')
    if form.validate_on_submit():
        #finds user in database with the username provided
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            #if user exists does password provided match the one in the database if so log them in
            if user.password == form.password.data:
                login_user(user)
                return redirect(url_for('index'))
                #user doesnt exist
        return redirect(url_for('error'))
    return render_template('login.html', title=':: Login', form=form)

@app.route('/transfer', methods=['GET', 'POST'])
@login_required
def transfer():
    form = TransferForm()
    user = current_user.get_id()
    if form.validate_on_submit():
        #checks to see if the reciever id exists if not error is thrown
        testuser = form.reciever.data
        test = User.query.filter_by(id = testuser).first()
        if test:
            #if the reciever exists then do this to check after deduction if the current user balance is bigger than overdraft
            temporary = current_user.balance
            temporary = temporary - form.amount.data
            overdrafttest = -current_user.overdraft
            #if the person tries to send money to their own id then throw error
            if str(form.reciever.data) == current_user.get_id():
                return redirect(url_for('error2'))
            #if person tries to send money which is of negative value
            elif form.amount.data < 0.00:
                return redirect(url_for('error3'))
            #if the temporary equation is bigger than overdraft then update and transfer the money else throw error
            elif (temporary >= overdrafttest):
                #money deducted from sender and updated in database
                newbalance = current_user.balance - form.amount.data
                balanceupdate = User.query.filter_by(id = current_user.get_id()).update(dict(balance = newbalance))
                db.session.commit()
                #query to find reciever in database, add money to their balance and update
                find = User.query.filter_by(id = testuser).first()
                newbalance2 = find.balance + form.amount.data
                balanceupdate2 = User.query.filter_by(id = testuser).update(dict(balance = newbalance2))
                db.session.commit()
                #log of transfer saved into transfer table
                transfer = Transfers(date = datetime.now(), message = form.message.data, transfersender = current_user.get_id(), transferreciever = form.reciever.data, amountsent = form.amount.data)
                db.session.add(transfer)
                db.session.commit()
                return redirect(url_for('sent'))
            else:
                return redirect(url_for('error4'))#insurfficent funds

        else:
            return redirect(url_for('error5'))#doesnt exists

    return render_template('transfer.html', title=':: Transfer', form=form)

@app.route('/withdraw', methods=['GET', 'POST'])
@login_required
def withdraw():
    form = WithdrawForm()
    user = current_user.get_id()
    if form.validate_on_submit():
        #if the reciever exists then do this to check after deduction if the current user balance is bigger than overdraft
        temporary = current_user.balance
        temporary = temporary - form.amount.data
        overdrafttest = -current_user.overdraft
        #if they try to withdraw negative value redirect to error page
        if form.amount.data < 0.00:
            return redirect(url_for('error3'))
        #if the temporary balance is bigger than overdraft meaning they have sufficient funds
        elif (temporary >= overdrafttest):
            #update balance by taking away amount
            newbalance = current_user.balance - form.amount.data
            balanceupdate = User.query.filter_by(id = current_user.get_id()).update(dict(balance = newbalance))
            db.session.commit()
            #log of this in withdrawl table
            withdraw = Withdrawls(date = datetime.now(), withdrawl = form.amount.data, withdrawluser = current_user.get_id())
            db.session.add(withdraw)
            db.session.commit()
            return redirect(url_for('successw'))
        else:
            return redirect(url_for('error4'))#insurfficent funds
    return render_template('withdraw.html', title=':: Withdraw', form=form)

@app.route('/deposit', methods=['GET', 'POST'])
@login_required
def deposit():
    form = DepositForm()
    user = current_user.get_id()
    if form.validate_on_submit():
        #if they try to deposit a negative amount
        if form.amount.data < 0.00:
            return redirect(url_for('error3'))
        else:
            #update the users balance by adding deposit amount
            newbalance = current_user.balance + form.amount.data
            balanceupdate = User.query.filter_by(id = current_user.get_id()).update(dict(balance = newbalance))
            db.session.commit()
            #make log of this in Deposits table
            deposit = Deposits(date = datetime.now(), deposit = form.amount.data, deposituser = current_user.get_id())
            db.session.add(deposit)
            db.session.commit()
            return redirect(url_for('successd'))
    return render_template('deposit.html', title=':: Deposit', form=form)

@app.route('/overview')
@login_required
def overview():
    #Query to find all Withdrawls,Deposits,Money Recieved and Sent for current logged in user
    overview = Withdrawls.query.filter_by(withdrawluser= current_user.get_id()).all()
    overview2 = Deposits.query.filter_by(deposituser = current_user.get_id()).all()
    overview3 = Transfers.query.filter_by(transfersender = current_user.get_id()).all()
    overview4 = Transfers.query.filter_by(transferreciever = current_user.get_id()).all()
    return render_template('overview.html', title=':: Overview', overview = overview, overview2 = overview2, overview3 = overview3, overview4 = overview4)
#render error page
@app.route('/error2')
@login_required
def error2():
    return render_template('error2.html', title=':: Error')
@app.route('/error3')
#render error page
@login_required
def error3():
    return render_template('error3.html', title=':: Error')

@app.route('/depositview')
@login_required
def depositview():
    #query to find all deposits made by current logged in user
    overview2 = Deposits.query.filter_by(deposituser = current_user.get_id()).all()
    return render_template('depositview.html', title=':: Deposit Views', overview2 = overview2)

@app.route('/withdrawlview')
@login_required
def withdrawlview():
    #query to find all withdrawls made by current logged in user
    overview = Withdrawls.query.filter_by(withdrawluser= current_user.get_id()).all()
    return render_template('withdrawlview.html', title=':: Withdrawls Views', overview = overview)

@app.route('/outgoing')
@login_required
def outgoing():
    #query to find all money sent by current logged in user
    overview3 = Transfers.query.filter_by(transfersender = current_user.get_id()).all()
    return render_template('outgoing.html', title=':: Outgoing Views', overview3 = overview3)

@app.route('/incoming')
@login_required
def incoming():
    #query to find all money recieved by current logged in user
    overview4 = Transfers.query.filter_by(transferreciever = current_user.get_id()).all()
    return render_template('incoming.html', title=':: Incoming Views', overview4 = overview4)

#render error page
@app.route('/exists')
def exists():
    return render_template('exists.html', title=':: Error')
#render error page
@app.route('/error4')
@login_required
def error4():
    return render_template('error4.html', title=':: Error')
#render error page
@app.route('/error5')
@login_required
def error5():
    return render_template('error5.html', title=':: Error')
#render successfull message page
@app.route('/sent')
@login_required
def sent():
    return render_template('sent.html', title=':: Sent', nbalance = current_user.balance)
#render successfull message page
@app.route('/successw')
@login_required
def successw():
    return render_template('successw.html', title=':: Withdraw', nb = current_user.balance)
#render successfull message page
@app.route('/successd')
@login_required
def successd():
    return render_template('successd.html', title=':: Deposit', nb = current_user.balance)
#render error page
@app.route('/notsame')
def notsame():
    return render_template('notsame.html', title=':: Error')
#render error page
@app.route('/error')
def error():
    return render_template('error.html', title=':: Error')
#render information page
@app.route('/about')
def about():
    return render_template('about.html', title=':: Zion Banking Group')
#render information page
@app.route('/')
def home():
    return render_template('home.html', title=':: Zion Banking Group')
#logout current logged in user
@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

#render greet user page and obtain their details to display
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
        return render_template('index.html', title=':: Home', account =current_user.accountype, fname = current_user.firstname, lname = current_user.lastname, balance = current_user.balance, overdraft = current_user.overdraft, uid = current_user.get_id())
