import os
import unittest
from app import app,db

app.config['WTF_CSRF_ENABLED'] = False

class TestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass
    #Test to see if the login page loads up correctly.
    def test_login(self):
        test = app.test_client(self)
        response = test.get('/login', content_type='html/text')
        self.assertTrue(b'Login to your bank account:' in response.data)
    #Test to see if the register page loads up correctly.
    def test_register(self):
        test = app.test_client(self)
        response = test.get('/register')
        self.assertTrue(b'Register' in response.data)
    #Test to see if the about page loads up correctly.
    def test_advice(self):
        test = app.test_client(self)
        response = test.get('/about', content_type='html/text')
        self.assertTrue(b'Student' in response.data)
    #Test to see if the home page loads up correctly.
    def test_home(self):
        test = app.test_client(self)
        response = test.get('/', content_type='html/text')
        self.assertTrue(b'Zion' in response.data)

    #Test to see if user is logged in given correct details.
    def test_loginbehavaiour(self):
        test = app.test_client(self)
        response = test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        self.assertIn(b'Mini Overview', response.data)

    #Test to see what happens when wrong details provided.
    def test_wrongbehavaiour(self):
        test = app.test_client(self)
        response = test.post('/login', data=dict(username="Fredoo", password="abbas786"), follow_redirects = True)
        self.assertIn(b'Incorrect', response.data)

    #Test to see if logout works
    def test_logout(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/logout', follow_redirects = True)
        self.assertIn(b'Login to your bank account:', response.data)

    #Test to see if the index page loads up correctly.
    def test_index(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/index', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Welcome' in response.data)
    #Test to see if deposit page loads up correctly
    def test_deposit(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/deposit', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Deposit' in response.data)
    #Test to see if withdraw page loads up correctly
    def test_withdraw(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/withdraw', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Withdraw' in response.data)
    #Test to see if transfer page loads up correctly
    def test_transfer(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/transfer', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Transfer' in response.data)
        #Test to see if settings page loads up correctly
    def test_settings(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/settings', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Username' in response.data)
        #Test to see if history page loads up correctly
    def test_histroy(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/overview', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'History' in response.data)
        #Test to see if change number page loads correctly
    def test_changenum(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/changeNum', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Change Number' in response.data)
        #Test to see if change address page loads correctly
    def test_changeaddress(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/changeAddress', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Change Address' in response.data)
        #Test to see if change age page loads correctly
    def test_changeage(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/changeage', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Change Age' in response.data)
        #Test to see if change password page loads correctly
    def test_changepassword(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.get('/changepass', content_type='html/text', follow_redirects = True)
        self.assertTrue(b'Change Password' in response.data)
        #Test to see if changing the current user age works
    def test_changeagetest(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/changeage', data=dict(age= "22"), follow_redirects = True)
        self.assertTrue(b'Success' in response.data)
        #Test to see if changing the current user address works
    def test_changeaddresstest(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/changeAddress', data=dict(address= "1 Test Street T16 9BP"), follow_redirects = True)
        self.assertTrue(b'Success' in response.data)
        #Test to see if changing the current user password works
    def test_changepasswordtest(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="TestAcc", password="TestOkayAgain"), follow_redirects = True)
        response = test.post('/changepass', data=dict(password= "Test123", password2 = "Test123"), follow_redirects = True)
        self.assertTrue(b'Success' in response.data)
        #Test to see if changing the current user contact number works
    def test_changenumtest(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/changeNum', data=dict(contactnum= "07823456789"), follow_redirects = True)
        self.assertTrue(b'Success' in response.data)
        #Test to see if registering a new user works
    def test_registeruser(self):
        test = app.test_client(self)
        response = test.post('/register', data=dict(username = "TestAccount", password = "abbas786", password2 = "abbas786", firstname = "Test", lastname = "Test", age = "20", address= "1 Test Street T16 9BP", accountype = "Student", contactnum = "078967894534", gender = "Male", startingbalance = "£1000"), follow_redirects = True)
        self.assertTrue(b'Login' in response.data)
        #Test to see if user can deposit money into their account
    def test_depositmoney(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/deposit', data=dict(amount = 100), follow_redirects = True)
        self.assertTrue(b'Success' in response.data)
        #Test to see if user can withdraw money from their account
    def test_withdrawmoney(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/withdraw', data=dict(amount = 100), follow_redirects = True)
        self.assertTrue(b'Success' in response.data)
        #Test to see if user can transfer money to another account
    def test_transfermoney(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/transfer', data=dict(amount = 100, reciever = 2), follow_redirects = True)
        self.assertTrue(b'Success' in response.data)
        #Test to see if trying to transfer money to yourself throws error page
    def test_transfermoney1(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/transfer', data=dict(amount = 100, reciever = 6), follow_redirects = True)
        self.assertTrue(b'Error' in response.data)
        #Test to see if trying to transer money to user id which doesnt exist throws Error
    def test_transfermoney2(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/transfer', data=dict(amount = 100, reciever = 100), follow_redirects = True)
        self.assertTrue(b'Error' in response.data)
        #Test to see if transfer money which goes over overdraft and balance throws error
    def test_transfermoney3(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/transfer', data=dict(amount = 100000000, reciever = 1), follow_redirects = True)
        self.assertTrue(b'Error' in response.data)
        #Test to see if transfer of negative amount of money throws error
    def test_transfermoney4(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/transfer', data=dict(amount = -100, reciever = 1), follow_redirects = True)
        self.assertTrue(b'Error' in response.data)
        #Test to try withdraw negative amount of money throws error
    def test_withdrawmoney1(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/withdraw', data=dict(amount = -100), follow_redirects = True)
        self.assertTrue(b'Error' in response.data)
        #Test to see if deposit negative amount of money throws error
    def test_depositmoney1(self):
        test = app.test_client(self)
        test.post('/login', data=dict(username="Fredo", password="abbas786"), follow_redirects = True)
        response = test.post('/deposit', data=dict(amount = -100), follow_redirects = True)
        self.assertTrue(b'Error' in response.data)





if __name__ == '__main__':
    unittest.main()
